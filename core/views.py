from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import *
from .forms import *


# create : views   for    stadium

class ListStadium(generic.View):
    template_name = "core/stadium/list_st.html"
    context = {}

    def get(self, request, *args, **kwargs):
        stadium = Stadium.objects.filter(status=True)
        self.context = {
            "stadiums": stadium
        }
        return render(request, self.template_name,self.context)


class AddStadium(generic.CreateView):
    template_name = "core/stadium/add_st.html"
    model = Stadium
    form_class = AddStadiumForm
    success_url = reverse_lazy("core:list_st")


class UpdateStadium(generic.UpdateView):
    template_name = "core/stadium/update_st.html"
    model = Stadium
    form_class = UpdateStadiumForm
    success_url = reverse_lazy('core:list_st')

class DeleteStadium(generic.DeleteView):
    template_name = "core/stadium/delete_st.html"
    model = Stadium
    success_url = reverse_lazy("core:list_st")




# create : views   for    City

class ListCity(generic.View):
    template_name = "core/city/list_city.html"
    context = {}

    def get(self, request, *args, **kwargs):
        city = City.objects.filter(status=True)
        self.context = {
            "citys": city
        }
        return render(request, self.template_name,self.context)

class AddCity(generic.CreateView):
    template_name = "core/city/add_city.html"
    model = City
    form_class = AddCityForm
    success_url = reverse_lazy("core:list_city")

class UpdateCity(generic.UpdateView):
    template_name = "core/city/update_city.html"
    model = City
    form_class = UpdateCityForm
    success_url = reverse_lazy("core:list_city")

class DeleteCity(generic.DeleteView):
    template_name = "core/city/delete_city.html"
    model = City
    success_url = reverse_lazy("core:list_city")


# create : views   for    Team

class ListTeam(generic.View):
    template_name = "core/team/list_tm.html"
    context = {}

    def get(self, request, *args, **kwargs):
        team =  Team.objects.filter(status=True)
        self.context = {
            "teams": team
        }
        return render(request, self.template_name,self.context)
    
class AddTeam(generic.CreateView):
    template_name = "core/team/add_tm.html"
    model = Team
    form_class = AddTeamForm
    success_url = reverse_lazy("core:list_tm")

class UpdateTeam(generic.UpdateView):
    template_name = "core/team/update_tm.html"
    model = Team
    form_class = UpdateTeamForm
    success_url = reverse_lazy("core/team/list_tm")

class DeleteTeam(generic.DeleteView):
    template_name = "core/team/delete_tm.html"
    model = Team
    success_url = reverse_lazy("core:list_tm")
