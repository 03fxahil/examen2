from django.urls import path
from . import views

app_name="core"

urlpatterns = [
    path('list/stadium/',views.ListStadium.as_view(), name="list_st"),
    path('add/stadium/',views.AddStadium.as_view(), name="add_st"),
    path('update/stadium/<int:pk>/', views.UpdateStadium.as_view(), name= "update_st"),
    path('delete/stadium/<int:pk>/', views.DeleteStadium.as_view(), name= "delete_st"),
    
    path('list/city/',views.ListCity.as_view(), name="list_city"),
    path('add/city/',views.AddCity.as_view(), name="add_city"),
    path('update/city/<int:pk>/', views.UpdateCity.as_view(), name= "update_city"),
    path('delete/city/<int:pk>/', views.DeleteCity.as_view(), name= "delete_city"),

    path('list/team/',views.ListTeam.as_view(), name="list_tm"),
    path('add/team/',views.AddTeam.as_view(), name="add_tm"),
    path('update/team/<int:pk>/', views.UpdateTeam.as_view(), name= "update_tm"),
    path('delete/team/<int:pk>/', views.DeleteTeam.as_view(), name= "delete_tm"),
]