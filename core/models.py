from django.db import models
from django.contrib.auth.models import User

# Models NFL

class Stadium(models.Model):
    stadium_name=models.CharField(max_length=64)
    status=models.BooleanField(default=True)
    timestamp=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.stadium_name
    

class City(models.Model):
    stadium=models.ForeignKey(Stadium, on_delete=models.CASCADE)
    city_name=models.CharField(max_length=64)
    status=models.BooleanField(default=True)
    timestamp=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.city_name

class Team(models.Model):
    city=models.ForeignKey(City, on_delete=models.CASCADE)
    stadium=models.ForeignKey(Stadium, on_delete=models.CASCADE)
    team_name=models.CharField(max_length=64)
    description=models.CharField(max_length=285)
    status=models.BooleanField(default=True)
    timestamp=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.team_name