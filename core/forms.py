from django import forms
from .models import *

# Form : Stadium

class AddStadiumForm(forms.ModelForm):
    class Meta:
        model = Stadium
        fields = "__all__"
        widgets = {
            "name_stadium": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter the name of the stadium"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
                         }



class UpdateStadiumForm(forms.ModelForm):
    class Meta:
        model = Stadium
        fields = "__all__"
        widgets = {
            "name_stadium": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter the name of the stadium"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
                         }



# Form : City

class AddCityForm(forms.ModelForm):
    class Meta:
        model = City
        fields = "__all__"
        widgets = {
            "stadium": forms.Select(attrs={"class":"form-select form-control"}),
            "city_name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter the name of the city"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            }

class UpdateCityForm(forms.ModelForm):
    class Meta:
        model = City
        fields = "__all__"
        widgets = {
            "stadium": forms.Select(attrs={"class":"form-select form-control"}),
            "city_name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter the name of the city"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            }
        


# Form : Team 

class AddTeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = "__all__"
        widgets = {
            "stadium": forms.Select(attrs={"class":"form-select form-control"}),
            "city": forms.Select(attrs={"class":"form-select form-control"}),
            "team_name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter the name of the team"}),
            "description": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter the description"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            }
        
class UpdateTeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = "__all__"
        widgets = {
            "stadium": forms.Select(attrs={"class":"form-select form-control"}),
            "city": forms.Select(attrs={"class":"form-select form-control"}),
            "team_name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter the name of the team"}),
            "description": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter the description"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            }