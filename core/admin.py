from django.contrib import admin
from .models import *

# Register your models here.
@admin.register(Stadium)
class StadiumAdmin(admin.ModelAdmin):
    list_display = [
        "stadium_name",
        "status",
    ]

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = [
        "stadium",
        "city_name",
        "status",
    ]

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = [
        "city",
        "stadium",
        "team_name",
        "description",
        "status",
    ]
